import React from 'react'

const Title = () => (
  <div className='d-flex w-100 flex-column'>
    <div className='w-100 p-3 shadow-sm'>
      <h4 className='w-100 text-center font-weight-bolder'>BakeryCostsApp</h4>
    </div>
    <div className='w-100 mt-3'>
      <h4 className='w-100 text-center font-weight-bolder'>
        CALCULADORA DE CUSTO DA PRODUÇÃO DE PÃES
      </h4>
      <h6 className='w-100 text-center font-weight-bolder'>
        PARA MICRO EMPREENDERORES INDIVIDUAIS
      </h6>
    </div>
  </div>
)

export default Title
